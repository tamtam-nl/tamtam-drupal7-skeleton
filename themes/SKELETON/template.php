<?php

require_once __DIR__ . '/template_suggestion/vendor/autoload.php';

function ***MACHINE_NAME***_preprocess_html(&$vars) {

}

function ***MACHINE_NAME***_preprocess_page(&$vars, $hook) {
  $status = drupal_get_http_header("status");

  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__' . str_replace('_', '__', $vars['node']->type);
  }

  if ($status == "404 Not Found") {
    $vars['theme_hook_suggestions'][] = 'page__404';
  }

  if ($status == "403 Forbidden") {
    $vars['theme_hook_suggestions'][] = 'page__403';
  }

  if( isset($vars['page']['content']['system_main']['form']) && $vars['page']['content']['system_main']['form']['#form_id'] === 'search_api_page_search_form' ) {
    $vars['theme_hook_suggestions'][] = 'page__search';
  }

  if (isset($vars['template_suggestion'])) {
    $class_name = template_suggestion_get_class_name($vars);
    $page = PageTemplateFactory::create($vars, $class_name);
    $page->configure();
    $vars = $page->get_vars();
  }
}

/**
 * Override or insert variables into the node template.
 */
function ***MACHINE_NAME***_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['node']->type . '__' . $variables['view_mode'];

  $variables['classes_array'][] = $variables['view_mode'];
}

function ***MACHINE_NAME***_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);

  foreach ($head_elements as $key => $element) {
    if( isset($element['#name']) && $element['#name'] === 'generator' ) {
      unset($head_elements[$key]);
    }

    if( isset($element['#name']) && $element['#name'] === 'shortlink' ) {
      unset($head_elements[$key]);
    }

    if( isset($element['#attributes']['rel']) && $element['#attributes']['rel'] === 'shortcut icon' ) {
      unset($head_elements[$key]);
    }
  }
}

function ***MACHINE_NAME***_css_alter(&$css) {
  //make a list of module css to remove
  $css_to_remove = array();
  //$css_to_remove[] = drupal_get_path('module','system').'/system.base.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.menus.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.messages.css';
  $css_to_remove[] = drupal_get_path('module', 'system') . '/system.theme.css';
  $css_to_remove[] = drupal_get_path('module', 'user') . '/user.css';
  $css_to_remove[] = drupal_get_path('module', 'field') . '/theme/field.css)';
  $css_to_remove[] = drupal_get_path('module', 'ctools') . '/css/ctools.css)';

  $css_to_remove[] = drupal_get_path('module', 'node') . '/node.css';
  $css_to_remove[] = drupal_get_path('module', 'views') . '/css/views.css';
  $css_to_remove[] = drupal_get_path('module', 'search') . '/search.css';
  $css_to_remove[] = drupal_get_path('module', 'webform') . '/css/webform.css';
  $css_to_remove[] = drupal_get_path('module', 'field_group') . '/horizontal-tabs/horizontal-tabs.css';
  $css_to_remove[] = drupal_get_path('module', 'date') . '/date_api/date.css';
  // now we can remove the contribs from the array
  foreach ($css_to_remove as $index => $css_file) {
    if(isset($css[$css_file])) {
      unset($css[$css_file]);
    }
  }

  uasort($css, 'drupal_sort_css_js');
  $i = 0;

  foreach ($css as $name => $style) {
    $css[$name]['weight'] = $i++;
    $css[$name]['group'] = CSS_DEFAULT;
    $css[$name]['every_page'] = FALSE;
  }
}

function ***MACHINE_NAME***_js_alter(&$javascripts) {
  $profile = drupal_get_path('profile', '***MACHINE_NAME***');

  $js_to_unset = array();

  $js_to_unset[] = 'misc/ui/jquery.ui.button.min.js';
  $js_to_unset[] = 'misc/ui/jquery.ui.draggable.min.js';
  $js_to_unset[] = 'misc/ui/jquery.ui.resizable.min.js';
  $js_to_unset[] = 'misc/ui/jquery.ui.dialog.min.js';

  $js_to_unset[] = $profile . '/modules/contrib/linkit/js/linkit.js';
  $js_to_unset[] = $profile . '/modules/contrib/linkit/editors/ckeditor/linkitDialog.js';

  foreach($js_to_unset as $js) {
    unset($javascripts[$js]);
  }

  // Experimental. Currently makes two JS aggregated files instead of 4, might cause issues with JS further down the line.
  uasort($javascripts, 'drupal_sort_css_js');
  $i = 0;

  foreach ($javascripts as $name => $script) {
    $javascripts[$name]['weight'] = $i++;
    $javascripts[$name]['group'] = JS_DEFAULT;
    $javascripts[$name]['every_page'] = FALSE;
  }
}

function ***MACHINE_NAME***_menu_breadcrumb_alter(&$active_trail, $item) {
  foreach( $active_trail as $index => &$trail ) {
    $trail['title'] = '<span itemprop="title">' . $trail['title'] . '</span>';
    $trail['localized_options']['html'] = TRUE;
    $trail['localized_options']['attributes']['itemprop'] = 'url';
  }
}

function ***MACHINE_NAME***_sdl_editor_item($variables) {
  if (empty($variables['informations']->player)) {
    return $variables['image'];
  }
  else {
    $player = $variables['informations']->player;

    $output = is_array($player) ? $player : array('#markup' => $player);

    return drupal_render($output);
  }
}

/**
 * Returns HTML for the legend of an atom.
 */
function ***MACHINE_NAME***_sdl_editor_legend($variables) {
  return "";
}


function ***MACHINE_NAME***_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : '<span class="icon-arrow-prev"></span>'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : '<span class="icon-arrow-next"></span>'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));


  if ($pager_total[$element] > 1) {
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-item', 'pager-previous'),
        'data' => $li_previous,
        'html' => TRUE,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-item', 'pager-current'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-item', 'pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-item', 'pager-next'),
        'data' => $li_next,
      );
    }

    return '<strong class="element-invisible">Paginering</strong>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

function ***MACHINE_NAME***_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
  }

  $attributes['href'] = url($_GET['q'], array('query' => $query));
  return '<a' . drupal_attributes($attributes) . ' rel="noindex, follow">' . $text . '</a>';
}

function ***MACHINE_NAME***_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $crumbs = '';

  array_shift($breadcrumb);

  if( count($breadcrumb) < 1 ) {
    return;
  }

  if (!empty($breadcrumb)) {
    $crumbs = '<ul class="breadcrumb">';

    foreach($breadcrumb as $value) {
      $value = str_replace('<a href', '<a itemprop="url" href', $value);
      $value = str_replace('">', '"><span itemprop="title">', $value);
      $value = str_replace('</a>', '</span></a>', $value);

      $crumbs .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$value.'</li>';
    }

    //$crumbs .= '<li><span class="current">' . drupal_get_title() . '</span></li></ul>';
    $crumbs .= '</ul>';
  }

  return $crumbs;
}

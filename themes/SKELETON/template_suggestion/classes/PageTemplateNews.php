<?php

class PageTemplateNews extends PageTemplate implements iPageTemplate {
    private $nids = [];

    public function configure() {
      $this->set_content();
    }

    private function set_content() {
        $this->vars['page']['content'] = $this->render_nodes($this->get_news_entities(), 'teaser');
    }

    private function get_news_entities() {
        $query = new EntityFieldQuery();

        $query->entityCondition('entity_type', 'node')
            ->propertyCondition('type', 'news')
            ->propertyCondition('status', 1)
            ->propertyOrderBy('created', 'DESC');

        $results = $query->execute();
        return entity_load('node', array_keys($results['node']));
    }
}

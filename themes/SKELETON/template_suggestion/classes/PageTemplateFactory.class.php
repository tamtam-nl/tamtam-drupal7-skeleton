<?php

abstract class PageTemplateFactory {
    public static function create(&$vars, $class_name) {
      if (class_exists($class_name)) {
        return new $class_name($vars);
      }
    }
}

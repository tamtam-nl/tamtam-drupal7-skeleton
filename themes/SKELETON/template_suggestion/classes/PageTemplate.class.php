<?php

class PageTemplate {
    protected $vars;

    public function __construct(array $vars) {
      $this->vars = $vars;
    }

    public function get_vars() {
      return $this->vars;
    }

    public function render_nodes(array $entities, $view_mode) {
        return entity_view('node', $entities, $view_mode);
    }
}

<?php print render($page['header']); ?>

<?php if( !empty($breadcrumb) ): ?>
  <nav id="breadcrumb">
    <?php print $breadcrumb; ?>
  </nav>
<?php endif; ?>

<main id="main" role="main">
  <?php print render($title_prefix); ?>
  <?php if( !empty($title) ): ?>
    <h1 class="title" id="page-title"><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php print render($page['content']); ?>
</main> <!-- /#main -->

<?php if( !empty($tabs) ): ?>
  <?php print render($tabs); ?>
<?php endif; ?>

<?php print render($page['footer']); ?>

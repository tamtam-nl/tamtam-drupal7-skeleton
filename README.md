# Generate a Drupal 7 base with Vagrant and Kraftwagen
One-stop solution for generating a base for a Drupal version 7 project. The script downloads and install all the necessities for a Drupal environment.

1. Download the script from [this](http://vagrant.tamtam.nl/drupal7-new-project.sh) link.
2. Run it

        ./drupal7-new-project.sh

3. Answer the questions.
  * Human readable name, used in Drupal settings: e.g. Lorem Ipsum Donec
  * Machine name, used in Drupal settings + generating folders + naming setting files: e.g. lorem-ipsum-donec
  * Development address, used for accessing the project through your browser (default: 33.33.33.10): e.g. lorem-ipsum-donec.dev
4. Wait for it..
5. ???
6. Profit!


The script downloads a Vagrantbox and installs Drupal by using Kraftwagen.

# But wait there's more!
We have a script available that generates a Drupal 7 environment from an existing project.

1. Download the script from [this](http://vagrant.tamtam.nl/drupal7-existing-project.sh) link.
2. Run it

        ./drupal7-existing-project.sh

3. Answer the questions.
  * GIT repository url
  * Machine name, used in Drupal settings + generating folders + naming setting files: e.g. lorem-ipsum-donec
  * Development address, used for accessing the project through your browser (default: 33.33.33.10): e.g. lorem-ipsum-donec.dev
4. Wait for it..
5. ???
6. More profit!


For more information:

- [Kraftwagen](kraftwagen.org/get-started.html)
- [Drush](http://www.drushcommands.com/)
- [Drupal](https://www.drupal.org/)

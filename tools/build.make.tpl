core = 7.x
api = 2

defaults[projects][l10n_url] = http://ftp.drupal.org/files/translations/l10n_server.xml

projects[drupal][type] = core
projects[drupal][version] = "7.39"

projects[drupal][patch][] = "http://patches.development.vdmi.nl/drupal/drupal_vhostalias.patch"
projects[drupal][patch][] = "http://patches.development.vdmi.nl/drupal/tk-patch-htaccess.patch"
projects[drupal][patch][] = "http://patches.development.vdmi.nl/drupal/tk-patch-private-file.patch"
projects[drupal][patch][1081266] = "https://www.drupal.org/files/issues/drupal-1081266-138-drupal_get_filename-D7.patch"
projects[drupal][patch][1162752] = "https://www.drupal.org/files/drupal-add_taxonomy_filter_to_content_admin-1162752-14.patch"
projects[drupal][patch][1878172] = "https://www.drupal.org/files/issues/guardr-remove-changelog-txt-1878172-55.patch"
projects[drupal][patch][] = "http://patch.tamtam.nl/deny_access_install_php.patch"
projects[drupal][patch][] = "http://patch.tamtam.nl/return_404_for_modules_directory.patch"
projects[drupal][patch][] = "http://patch.tamtam.nl/htaccess-add-x-frame-options-and-common-csp.patch"

projects[***MACHINE_NAME***][type] = "profile"
projects[***MACHINE_NAME***][download][type] = "kraftwagen_directory"
projects[***MACHINE_NAME***][download][url] = "**SRC_DIR**"

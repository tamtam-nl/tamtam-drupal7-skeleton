<?php

$databases = array (
  'default' => array (
    'default' => array (
      'database' => 'vagrant',
      'username' => 'root',
      'password' => 'root',
      'host'     => 'localhost',
      'port'     => '',
      'driver'   => 'mysql',
      'prefix'   => '',
    ),
  ),
);

$drupal_hash_salt = 'SOME_REALLY_RANDOM_STRING';

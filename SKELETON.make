core                                    = 7.x
api                                     = 2

translations[] = nl

defaults[projects][subdir]              = contrib
defaults[projects][l10n_url] = http://ftp.drupal.org/files/translations/l10n_server.xml

projects[kw_manifests][type]            = "module"
projects[kw_manifests][download][type]  = "git"
projects[kw_manifests][download][url]   = "git://github.com/kraftwagen/kw-manifests.git"
projects[kw_manifests][subdir]          = "kraftwagen"

projects[kw_itemnames][type]            = "module"
projects[kw_itemnames][download][type]  = "git"
projects[kw_itemnames][download][url]   = "git://github.com/kraftwagen/kw-itemnames.git"
projects[kw_itemnames][subdir]          = "kraftwagen"

projects[admin_menu][version] = "3.0-rc5"

projects[adminimal_admin_menu][version] = "1.6"

projects[advanced_link][version] = "1.3"

projects[better_formats][version] = "1.0-beta1"

projects[breakpoints][version] = "1.3"

projects[ctools][version] = "1.9"

projects[conditional_styles][version] = "2.2"

projects[context][version] = "3.6"

projects[login_destination][version] = "1.1"

projects[features][version] = "2.6"

projects[strongarm][version] = "2.0"

projects[ckeditor][version] = "1.16"

projects[date][version] = "2.9"

projects[diff][version] = "3.2"

projects[entity][version] = "1.6"

projects[entityreference][version] = "1.1"

projects[filecache][version] = "1.0-beta2"

projects[find_content][version] = "1.6"

projects[picture][version] = "2.13"

projects[form_builder][version] = "1.13"

projects[globalredirect][version] = "1.5"

projects[honeypot][version] = "1.21"

projects[hms_field][version] = "1.2"

projects[imageapi_optimize][version] = "1.2"

projects[jquery_update][version] = "3.0-alpha2"

projects[libraries][version] = "2.2"

projects[link][version] = "1.3"

projects[linkit][version] = "3.4"

projects[mailsystem][version] = "2.34"

projects[mimemail][version] = "1.0-beta3"

projects[menu_attributes][version] = "1.0-rc3"

projects[menu_trail_by_path][version] = "2.0"

projects[metatag][version] = "1.7"

projects[multiple_selects][version] = "1.2"

projects[options_element][version] = "1.12"

projects[password_policy][version] = "1.12"

projects[path_redirect_import][version] = "1.0-rc4"

projects[pathauto][version] = "1.3"

projects[pathcache][version] = "1.1-beta1"

projects[plupload][version] = "1.7"

projects[redirect][version] = "1.0-rc3"

projects[role_export][version] = "1.0"

projects[roleassign][version] = "1.0"

projects[scald][version] = "1.5"

projects[scald_file][version] = "1.2"

projects[scald_youtube][version] = "1.5"

projects[scheduler][version] = "1.3"

projects[search_api][version] = "1.16"

projects[search_api_db][version] = "1.5"

projects[search_api_page][version] = "1.2"

projects[smart_trim][version] = "1.5"

projects[token][version] = "1.6"

projects[transliteration][version] = "3.2"

projects[field_group][version] = "1.4"

projects[variable][version] = "2.5"

projects[video_filter][version] = "3.1"

projects[views][version] = "3.11"

projects[webform][version] = "4.11"

projects[xmlsitemap][version] = "2.2"

projects[stage_file_proxy][subdir] = 'devel'
projects[stage_file_proxy][version] = "1.7"

projects[devel][subdir] = 'devel'
projects[devel][version] = "1.5"

projects[search_krumo][subdir] = 'devel'
projects[search_krumo][version] = "1.6"

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.8/ckeditor_4.4.8_standard.zip"

; Themes
projects[adminimal_theme][version] = "1.22"

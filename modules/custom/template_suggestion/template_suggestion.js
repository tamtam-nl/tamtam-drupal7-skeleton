(function ($) {

Drupal.behaviors.templateSuggestionFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.template-suggestion-form', context).drupalSetSummary(function (context) {
      return Drupal.checkPlain($('.form-item-template-suggestion select option:selected').text());
    });
  }
};

})(jQuery);

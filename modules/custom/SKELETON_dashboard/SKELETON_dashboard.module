<?php

function ***MACHINE_NAME***_dashboard_block_info() {
  $blocks = array();

  $blocks['mycontent'] = array(
    'info' => t('View  own last 30 content items'),
    'status' => 1,
    'region' => 'dashboard_main',
    'weight' => 0,
    'cache' => DRUPAL_NO_CACHE,
  );

  $blocks['unpublishedcontent'] = array(
    'info' => t('View own last 30 unpublished content items'),
    'status' => 1,
    'region' => 'dashboard_sidebar',
    'weight' => 0,
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

function ***MACHINE_NAME***_dashboard_block_view($delta) {
  $block = array();

  if( $delta === 'mycontent' ) {
    $block['subject'] = t('My recent content');
    $block['content'] = get_content_by_owner_table();
  }

  if( $delta === 'unpublishedcontent' ) {
    $block['subject'] = t('Recently unpublished content');
    $block['content'] = get_content_unpublished_table();
  }

  return $block;
}


function get_content_by_owner_table() {
  $languages = language_list();

  $rows = array();

  $header = array(
    array('data' => t('Title')),
    array('data' => t('Type')),
    array('data' => t('Status')),
    array('data' => t('Language')),
    array('data' => t('Operations')),
  );

  foreach( get_nodes_by_owner() as $node ) {
    $rows[] = array(
      'data' => array(
          l($node->title, 'node/'. $node->nid),
          $node->type,
          $node->status == NODE_PUBLISHED ? t('published') : t('unpublished'),
          isset($languages[$node->language]) ? t($languages[$node->language]->name) : '',
          l(t('edit'), 'node/'. $node->nid .'/edit') . ' ' . l(t('delete'), 'node/'. $node->nid .'/delete')
      )
    );
  }

  $html = theme('table', array(
    'header' => $header,
    'rows'=>$rows,
    'sticky' => TRUE,
    'empty' => t('You have no recent content history'),
  ));

  return $html;
}

function get_nodes_by_owner() {
  global $user;

  $nodes = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
        ->propertyCondition('uid', $user->uid)
        ->range(0,30)
        ->propertyOrderBy('changed', 'DESC');

  $results = $query->execute();

  if(!empty($results)) {
    $nodes = entity_load('node', array_keys($results['node']));
  }

  return $nodes;
}


function get_content_unpublished_table() {
  $languages = language_list();

  $rows = array();

  $header = array(
    array('data' => t('Title')),
    array('data' => t('Type')),
    array('data' => t('Author')),
    array('data' => t('Operations')),
  );

  foreach( get_nodes_unpublished() as $node ) {
    $rows[] = array(
        'data' => array(
            l($node->title, 'node/'. $node->nid),
            $node->type,
            $node->name,
            l(t('edit'), 'node/'. $node->nid .'/edit') . ' ' . l(t('delete'), 'node/'. $node->nid .'/delete')
        )
    );
  }

  $html = theme('table', array(
    'header' => $header,
    'rows'=>$rows,
    'sticky' => TRUE,
    'empty' => 'There is no unpublished content',
  ));

  return $html;
}

function get_nodes_unpublished() {
  global $user;

  $nodes = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
        ->propertyCondition('status', 0)
        ->range(0,30)
        ->propertyOrderBy('changed', 'DESC');

  $results = $query->execute();

  if(!empty($results)) {
    $nodes = entity_load('node', array_keys($results['node']));
  }

  return $nodes;
}

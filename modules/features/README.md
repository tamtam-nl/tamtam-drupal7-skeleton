This directory contains the feature modules for ***HUMAN_NAME***.

All feature modules should be prefixed with ***MACHINE_NAME***_ft_.

Please note that 'feature module' doesn't necessarily mean that the module is
managed by the Features module. It means that the module exposes a specific 
feature, instead of being a generic implementation of some functionality. 

<?php
/**
 * @file
 * ft_scald.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ft_scald_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scald_context_fallbacks';
  $strongarm->value = array(
    '@default' => array(
      0 => 'title',
    ),
  );
  $export['scald_context_fallbacks'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scald_switch_feature_export';
  $strongarm->value = TRUE;
  $export['scald_switch_feature_export'] = $strongarm;

  return $export;
}

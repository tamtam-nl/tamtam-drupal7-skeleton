<?php
/**
 * @file
 * ft_user.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ft_user_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
    'machine_name' => 'administrator',
  );

  // Exported role: customer.
  $roles['customer'] = array(
    'name' => 'customer',
    'weight' => 3,
    'machine_name' => 'customer',
  );

  return $roles;
}

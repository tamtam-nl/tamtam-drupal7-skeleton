<?php
/**
 * @file
 * ft_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ft_news_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ft_news_node_info() {
  $items = array(
    'news' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use to create articles'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
